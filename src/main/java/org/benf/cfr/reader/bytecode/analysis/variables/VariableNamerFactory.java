package org.benf.cfr.reader.bytecode.analysis.variables;

import org.benf.cfr.reader.entities.attributes.AttributeLocalVariableTable;
import org.benf.cfr.reader.entities.constantpool.ConstantPool;

public class VariableNamerFactory {
	private VariableNamerFactory()
    {
    }

    public static VariableNamer getNamer(AttributeLocalVariableTable source, ConstantPool cp)
    {
        return new VariableNamerDefault();
    }
}
