package org.benf.cfr.reader.entities;

import java.util.ArrayList;
import java.util.List;

import org.benf.cfr.reader.bytecode.analysis.types.JavaTypeInstance;
import org.benf.cfr.reader.util.output.ToStringDumper;
import org.boobin.jardiffanalyzer.processing.data.entities.ClassData;
import org.boobin.jardiffanalyzer.processing.data.entities.FieldData;
import org.boobin.jardiffanalyzer.processing.data.entities.MethodData;

class CfrClassData implements ClassData {
	private static final long serialVersionUID = 1L;
	
	private String className;
	private String superClassName;
	private boolean interf;
	private String rawCode;
	private String[] interfNames;
	
	private List<MethodData> methods = new ArrayList<>();
	private List<FieldData> fields = new ArrayList<>();
	
	CfrClassData(ClassFile cl) {
		className = cl.getClassType().getRawName();
		superClassName = cl.getBaseClassType().getRawName();
		interf = cl.getAccessFlags().contains(AccessFlag.ACC_INTERFACE);
		
		for(Method m : cl.getMethods()) {
			if(!m.testAccessFlag(AccessFlagMethod.ACC_SYNTHETIC)) {
				methods.add(new CfrMethodData(m));
			}
		}
		for(ClassFileField f : cl.getFields()) {
			if(!f.getField().testAccessFlag(AccessFlag.ACC_SYNTHETIC)) {
				fields.add(new CfrFieldData(f));
			}
		}
		int nbInt = cl.getClassSignature().getInterfaces().size();
		interfNames = new String[nbInt];
		int i = 0;
		for(JavaTypeInstance interf : cl.getClassSignature().getInterfaces()) {
			interfNames[i] = interf.getRawName();
			i++;
		}
		rawCode = ToStringDumper.toString(cl);
	}

	@Override
	public String getClassName() {
		return className;
	}

	@Override
	public List<MethodData> getMethods() {
		return methods;
	}

	@Override
	public List<FieldData> getFields() {
		return fields;
	}

	@Override
	public String getSuperclassName() {
		return superClassName;
	}

	@Override
	public String[] getInterfaceNames() {
		return interfNames;
	}

	@Override
	public boolean isInterface() {
		return interf;
	}
	
	@Override
	public String getRawCode() {
		return rawCode;
	}
	
	@Override
	public String cleanCode(String rawCode) {
		return rawCode;
	}
	
}
