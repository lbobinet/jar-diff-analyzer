package org.benf.cfr.reader.entities;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.benf.cfr.reader.state.ClassFileSourceImpl;
import org.benf.cfr.reader.state.DCCommonState;
import org.benf.cfr.reader.util.bytestream.BaseByteData;
import org.benf.cfr.reader.util.getopt.Options;
import org.benf.cfr.reader.util.getopt.OptionsImpl;
import org.boobin.jardiffanalyzer.processing.data.entities.ClassData;
import org.boobin.jardiffanalyzer.processing.data.entities.ClassDataFactory;

public class CfrClassDataFactory implements ClassDataFactory {

	private DCCommonState state;
	private ClassLoader cl;
	
	public CfrClassDataFactory(ClassLoader cl, File jarFile) {
		this.cl = cl;
		Options opt = new OptionsImpl(new HashMap<>());
		ClassFileSourceImpl classFileSource = new ClassFileSourceImpl(opt);
		classFileSource.addJar(jarFile.getAbsolutePath());
		state = new DCCommonState(opt, classFileSource);
	}
	
	@Override
	public ClassData newClassData(String name) {
		InputStream is = cl.getResourceAsStream(name.replace('.', '/') + ".class");
		try {
			byte[] bytes = IOUtils.toByteArray(is);
			ClassFile cf = new ClassFile(new BaseByteData(bytes), "", state);
			return new CfrClassData(cf);
		} catch(Exception e) {
			System.err.println("Error while parsing " + name);
			e.printStackTrace();
		}
		return null;
	}
	
}
