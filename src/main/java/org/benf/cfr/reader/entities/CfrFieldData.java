package org.benf.cfr.reader.entities;

import org.boobin.jardiffanalyzer.processing.data.entities.FieldData;

public class CfrFieldData implements FieldData {
	private static final long serialVersionUID = 1L;
	
	private final String name;
	private final String type;
	private final String accessFlags;
	
	CfrFieldData(ClassFileField f) {
		name = f.getFieldName();
		type = f.getField().getJavaTypeInstance().getRawName();
		
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(AccessFlag af : AccessFlag.values()) {
			if(f.getField().testAccessFlag(af)) {
				if(!first) {
					sb.append(" ");
				} else {
					first = false;
				}
				sb.append(af.toString());
			}
		}
		accessFlags = sb.toString();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public String getAccessFlags() {
		return accessFlags;
	}
	
}
