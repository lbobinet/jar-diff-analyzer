package org.benf.cfr.reader.entities;


import org.benf.cfr.reader.bytecode.analysis.types.JavaTypeInstance;
import org.benf.cfr.reader.util.output.ToStringDumper;
import org.boobin.jardiffanalyzer.processing.data.entities.MethodData;

public class CfrMethodData implements MethodData {
	private static final long serialVersionUID = 1L;

	private String returnType;
	private String name;
	private String signature;
	private String accessFlags;
	private String rawCode;
	private String[] argTypes;

	CfrMethodData(Method delegate) {
		name = delegate.getName();

		if(!delegate.getMethodPrototype().parametersComputed()) {
			try {
				delegate.analyse();
			} catch(Exception e) {
				System.err.println("Error while analyzing " + getName());
				e.printStackTrace();
			}
		}
		
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(AccessFlagMethod afm : delegate.getAccessFlags()) {
			if(!first) {
				sb.append(" ");
			} else {
				first = false;
			}
			sb.append(afm.toString());
		}
		accessFlags = sb.toString();
		
		if (delegate.getMethodPrototype().getReturnType() == null) {
			returnType = "void";
		} else {
			returnType = delegate.getMethodPrototype().getReturnType().getRawName();
		}
		signature = accessFlags + " " + returnType + " "
				+ delegate.getMethodPrototype().toString();		

		int nbArgs = delegate.getMethodPrototype().getArgs().size();
		argTypes = new String[nbArgs];
		int i = 0;
		for(JavaTypeInstance t : delegate.getMethodPrototype().getArgs()) {
			argTypes[i] = t.getRawName();
			i++;
		}
		
		ToStringDumper d = new ToStringDumper();
		delegate.dump(d, true);
		rawCode = d.toString();
	}

	@Override
	public String getReturnType() {
		return returnType;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String[] getArgumentTypes() {
		return argTypes;
	}

	@Override
	public String getSignature() {
		return signature;
	}

	@Override
	public String getAccessFlags() {
		return accessFlags;
	}

	@Override
	public String getDataToCompare() {
		return rawCode;
	}
	
	@Override
	public String cleanCode(String rawCode) {
		return rawCode;
	}

	@Override
	public String getRawCode(String classRawCode) {
		return rawCode;
	}

}
