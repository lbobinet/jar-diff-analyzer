package org.boobin.jardiffanalyzer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.boobin.jardiffanalyzer.processing.ClassProcessing;
import org.boobin.jardiffanalyzer.processing.ClassProcessing.ClassFilter;
import org.boobin.jardiffanalyzer.processing.ClassProcessing.NajaClassFilter;
import org.boobin.jardiffanalyzer.processing.ClassProcessing.NoClassFilter;

public class ReportGenerator {

	public static void main(final String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage : source dest out.txt");
			System.exit(0);
		}
		File f1 = new File(args[0]);
		File f2 = new File(args[1]);

		if (!f1.exists() || !f2.exists()) {
			System.out.println("2 existing paths required");
			System.exit(0);
		}
		Map<String, Class<?>[]> classMap = new TreeMap<>();
		ClassProcessing.parseClass(classMap, f1, 0);
		ClassProcessing.parseClass(classMap, f2, 1);

		List<String> addedClasses = new ArrayList<>();
		List<String> removedClasses = new ArrayList<>();
		Map<String, List<String>> differentHierarchy = new TreeMap<>();
		Map<String, List<String>> differentMethodsClasses = new TreeMap<>();
		Map<String, List<String>> differentFieldsClasses = new TreeMap<>();

		ClassFilter filter = new NoClassFilter();
		if(System.getProperty("naja") != null)
		{
			filter = new NajaClassFilter();
		}
		ClassProcessing.compute(classMap, addedClasses,
				removedClasses, differentHierarchy, differentFieldsClasses,
				differentMethodsClasses, filter);
		ClassProcessing.writeReport(args[2], addedClasses, removedClasses,
				differentHierarchy, differentFieldsClasses,
				differentMethodsClasses);
	}
}
