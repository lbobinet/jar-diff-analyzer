package org.boobin.jardiffanalyzer;

import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Settings {

	public static final Color STRUCTURE_CHANGED_COLOR = new Color(255, 100, 100);
	public static final Color IMPLEM_CHANGED_COLOR = Color.ORANGE;
	public static final Color IDENTICAL_COLOR = Color.GREEN;
	public static final Color ADD_OR_REMOVE_COLOR = Color.YELLOW;

	public static class PackageSubstitution {
		public String before;
		public String after;
	}

	private static final String DEFAULT_SETTINGS_FILE = System
			.getProperty("user.home") + "/.javacomparator/settings";

	private List<PackageSubstitution> packageSubstitutions = new ArrayList<>();
	private String srcPath;
	private String destPath;
	private int width = 1024;
	private int height = 768;

	public List<PackageSubstitution> getPackageSubstitutions() {
		return packageSubstitutions;
	}

	public void setPackageSubstitutions(
			List<PackageSubstitution> packageSubstitutions) {
		this.packageSubstitutions = packageSubstitutions;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath.replace('\\', '/');
	}

	public String getDestPath() {
		return destPath;
	}

	public void setDestPath(String destPath) {
		this.destPath = destPath.replace('\\', '/');
	}

	public void importDefault() throws IOException {
		importFrom(new File(DEFAULT_SETTINGS_FILE));
	}

	private void importFrom(File file) throws IOException {
		if (file.exists()) {
			Properties prop = new Properties();
			FileReader fr = new FileReader(file);
			prop.load(fr);
			srcPath = prop.getProperty("$src$");
			destPath = prop.getProperty("$dest$");
			String widthStr = prop.getProperty("$width$");
			String heightStr = prop.getProperty("$height$");
			if(widthStr != null) {
				width = Integer.valueOf(widthStr);
			}
			if(heightStr != null) {
				height = Integer.valueOf(heightStr);
			}
			for (Object before : prop.keySet()) {
				if (!before.toString().startsWith("$")) {
					PackageSubstitution ps = new PackageSubstitution();
					ps.before = before.toString();
					ps.after = prop.getProperty(before.toString());
					packageSubstitutions.add(ps);
				}
			}
			fr.close();
		}
	}

	public void exportDefault(int width, int height) throws IOException {
		exportTo(new File(DEFAULT_SETTINGS_FILE), width, height);
	}

	private void exportTo(File file, int width, int height) throws IOException {
		if (file.exists()) {
			boolean success = file.delete();
			if (!success) {
				throw new IOException("Can't delete old settings file");
			}
		}
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		file.createNewFile();

		FileWriter fw = new FileWriter(file);
		fw.write("$width$=" + width + "\n");
		fw.write("$height$=" + height + "\n");
		if(srcPath != null)
		{
			fw.write("$src$=" + srcPath + "\n");
		}
		if(destPath != null)
		{
			fw.write("$dest$=" + destPath + "\n");
		}
		for (PackageSubstitution subst : packageSubstitutions) {
			fw.write(subst.before + "=" + subst.after + "\n");
		}
		fw.close();
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}

	public static String fixPackages(String input,
			List<PackageSubstitution> subst) {
		if(input != null) {
			String result = input.toLowerCase();
			for (PackageSubstitution sub : subst) {
				result = result.replaceAll(sub.before, sub.after);
			}
			return result;
		} else {
			return null;
		}
	}
}
