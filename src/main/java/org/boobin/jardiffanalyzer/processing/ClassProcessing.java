package org.boobin.jardiffanalyzer.processing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ClassProcessing {

	public static ClassLoader parseClass(
			final Map<String, Class<?>[]> classMap, final File file, final int index)
			throws IOException {
		ClassLoader cl = new URLClassLoader(new URL[] { file.toURI().toURL() });
		if (file.isDirectory()) {
			ClassProcessing
					.parseClassInFileSystem(classMap, file, index, cl, "", true);
		} else if (file.getName().endsWith(".jar")) {
			ClassProcessing.parseClassInJar(classMap, file, index, cl);
		}
		return cl;
	}

	public static void parseClassInJar(
			final Map<String, Class<?>[]> classMap, final File file, final int index,
			final ClassLoader classLoader) throws IOException {
		ZipFile zipFile = new ZipFile(file);
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if (entry.getName().endsWith(".class")) {
				String tmp = entry.getName().replaceAll("/", ".");
				String className = tmp.substring(0, tmp.lastIndexOf("."));
				ClassProcessing.loadClass(classMap, index, className, classLoader);
			}
		}
		zipFile.close();
	}

	private static void parseClassInFileSystem(
			final Map<String, Class<?>[]> classMap, final File file, final int index, 
			final ClassLoader classLoader, String packageName,
			final boolean isRoot) {
		if (file.isDirectory()) {
			if (!isRoot) {
				packageName += file.getName() + ".";
			}
			for (File child : file.listFiles()) {
				ClassProcessing.parseClassInFileSystem(classMap, child, index,
						classLoader, packageName, false);
			}
		} else if (file.getName().endsWith(".class")) {
			String className = packageName
					+ file.getName().substring(0, file.getName().indexOf("."));
			ClassProcessing.loadClass(classMap, index, className, classLoader);
		}
	}

	private static void loadClass(final Map<String, Class<?>[]> classMap, final int index,
			final String className, final ClassLoader classLoader) {
		try {
			Class<?> cl = Class.forName(className, false, classLoader);
			String idClass = idClass(cl);
			if (classMap.get(idClass) == null) {
				classMap.put(idClass, new Class<?>[2]);
			}
			classMap.get(idClass)[index] = cl;
		} catch (ClassNotFoundException e) {
			System.err.println("Problem while loading class " + className
					+ " : " + e.getLocalizedMessage());
		}
	}

	public static ClassLoader extractClasses(final File file, final int index,
			List<Class<?>> result) throws IOException {
		Map<String, Class<?>[]> content = new HashMap<>();
		ClassLoader cl = parseClass(content, file, index);
		for (Class<?>[] classes : content.values()) {
				result.addAll(Arrays.asList(classes));
		}
		return cl;
	}

	private static String idClass(Class<?> classe) {
		String result = classe.getName().toLowerCase();
		if (System.getProperty("remove.tas.package") != null) {
			result = result.replaceAll("\\.tas\\.idaf\\.", ".amascos.");
			result = result.replaceAll("\\.meltem\\.", ".amascos.");
			result = result.replaceAll("\\.tas\\.", ".");
			result = result.replaceAll("\\.pics\\.", ".");
		}
		return result;
	}

	private static String idField(Field field) {
		return field.getName() + "(" + idClass(field.getType()) + ")";
	}

	public static String idMethod(Method method) {
		StringBuilder result = new StringBuilder();
		result.append(method.getReturnType().getSimpleName());
		result.append(" " + method.getName());
		result.append("(");
		boolean first = true;
		for (Class<?> paramType : method.getParameterTypes()) {
			if (first) {
				first = false;
			} else {
				result.append(", ");
			}
			result.append(paramType.getSimpleName());
		}
		result.append(")");
		return result.toString();
	}

//	private static String extractPartialClassName(Class<?> classe, int depth) {
//		if (classe.isAnonymousClass()) {
//			return extractPartialClassName(classe.getEnclosingClass(), depth)
//					+ "*" + classe.hashCode();
//		}
//		StringBuilder result = new StringBuilder();
//		result.append(classe.getSimpleName());
//		String packageName = classe.getPackage().getName();
//		for (int i = 0; i < depth; i++) {
//			if (packageName.isEmpty()) {
//				break;
//			}
//			int index = packageName.lastIndexOf('.');
//			result.insert(0, packageName.substring(index + 1) + ".");
//			if (index == -1) {
//				packageName = "";
//			}
//			packageName = packageName.substring(0, index);
//		}
//		return result.toString();
//	}

	public static int compute(final Map<String, Class<?>[]> classMap,
			final List<String> addedClasses, final List<String> removedClasses,
			final Map<String, List<String>> differentHierarchy,
			final Map<String, List<String>> differentFieldsClasses,
			final Map<String, List<String>> differentMethodsClasses) {
		return ClassProcessing.compute(classMap, addedClasses,
				removedClasses, differentHierarchy, differentFieldsClasses,
				differentMethodsClasses, new NoClassFilter());
	}

	public static int compute(final Map<String, Class<?>[]> classMap,
			final List<String> addedClasses, final List<String> removedClasses,
			final Map<String, List<String>> differentHierarchy,
			final Map<String, List<String>> differentFieldsClasses,
			final Map<String, List<String>> differentMethodsClasses,
			final ClassFilter filter) {
		int nbErrors = 0;
		List<String> matchedClasses = new ArrayList<>();
		for (String className : classMap.keySet()) {
			Class<?>[] candidates = classMap.get(className);
			
			if (candidates[0] == null) {
				addedClasses.add(candidates[1].toString());
			} else if (candidates[1] == null) {
				removedClasses.add(candidates[0].toString());
			} else {
				Class<?> class1 = candidates[0];
				Class<?> class2 = candidates[1];

				if (filter.accept(class1) || filter.accept(class2)) {
					matchedClasses.add(className);
					List<String> methodDiff = new ArrayList<>();
					List<String> fieldDiff = new ArrayList<>();
					List<String> hierarchyDiff = new ArrayList<>();

					try {
						ClassProcessing.compareMethods(class1, class2,
								methodDiff, "-;");
						ClassProcessing.compareMethods(class2, class1,
								methodDiff, "+;");
						ClassProcessing.compareFields(class1, class2,
								fieldDiff, "-;");
						ClassProcessing.compareFields(class2, class1,
								fieldDiff, "+;");
						ClassProcessing.compareHierarchy(class1, class2,
								hierarchyDiff, "-;");
						ClassProcessing.compareHierarchy(class2, class1,
								hierarchyDiff, "+;");
					} catch (NoClassDefFoundError e) {
						System.err.println("Problem while reading class "
								+ className + " : " + e.getLocalizedMessage());
						nbErrors++;
					}

					if (!methodDiff.isEmpty()) {
						differentMethodsClasses.put(class1.toString(),
								methodDiff);
					}
					if (!fieldDiff.isEmpty()) {
						differentFieldsClasses
								.put(class1.toString(), fieldDiff);
					}
					if (!hierarchyDiff.isEmpty()) {
						differentHierarchy
								.put(class1.toString(), hierarchyDiff);
					}
				}
			}
		}

		System.out.println(matchedClasses.size() + " matched classes");
		System.out.println(removedClasses.size() + " removed classes");
		System.out.println(addedClasses.size() + " added classes");
		System.out.println(nbErrors + " errors");

		return nbErrors;
	}

	private static void compareHierarchy(final Class<?> class1,
			final Class<?> class2, final List<String> results,
			final String prefix) {
		List<Class<?>> types1 = new ArrayList<>();
		if (class1.getSuperclass() != null) {
			types1.add(class1.getSuperclass());
		}
		types1.addAll(Arrays.asList(class1.getInterfaces()));
		List<Class<?>> types2 = new ArrayList<>();
		if (class2.getSuperclass() != null) {
			types2.add(class2.getSuperclass());
		}
		types2.addAll(Arrays.asList(class2.getInterfaces()));
		for (Class<?> m1 : types1) {
			boolean found = false;
			for (Class<?> m2 : types2) {
				if (idClass(m1).equals(idClass(m2))) {
					found = true;
					break;
				}
			}
			if (!found) {
				// exclude naja components interfaces (deprecated and replaced
				// by annotations)
				if (!m1.getName().startsWith(
						"com.thalesgroup.naja.core.components")
						&& !m1.equals(Object.class)) {
					results.add(prefix + m1.getName());
				}
			}
		}
	}

	private static void compareMethods(final Class<?> class1,
			final Class<?> class2, final List<String> results,
			final String prefix) {
		for (Method m1 : class1.getDeclaredMethods()) {
			boolean found = false;
			String idMethod = idMethod(m1);
			for (Method m2 : class2.getDeclaredMethods()) {
				if (idMethod.equals(idMethod(m2))) {
					found = true;
					break;
				}
			}
			if (!found) {
				results.add(prefix + idMethod);
			}
		}
	}

	private static void compareFields(final Class<?> class1,
			final Class<?> class2, final List<String> results,
			final String prefix) {
		for (Field m1 : class1.getDeclaredFields()) {
			boolean found = false;
			String idField = idField(m1);
			for (Field m2 : class2.getDeclaredFields()) {
				if (idField.equals(idField(m2))) {
					found = true;
					break;
				}
			}
			if (!found) {
				results.add(prefix + idField);
			}
		}
	}

	public static void writeReport(final String rName,
			final List<String> addedClasses, final List<String> removedClasses,
			final Map<String, List<String>> differentHierarchy,
			final Map<String, List<String>> differentFieldsClasses,
			final Map<String, List<String>> differentMethodsClasses)
			throws IOException {
		String reportName = rName + ".csv";
		System.out.println(reportName);
		File out = new File(reportName);
		if (out.exists() && !out.isDirectory()) {
			out.delete();
		}
		boolean success = out.createNewFile();
		if (!success) {
			System.out.println("Can't create report file");
			return;
		}

		FileWriter fw = new FileWriter(out);

		fw.write("-------------- added classes --------------\n\n");
		for (String className : addedClasses) {
			fw.write("+;" + className + "\n");
		}
		fw.write("\n\n-------------- missing classes --------------\n\n");
		for (String className : removedClasses) {
			fw.write("-;" + className + "\n");
		}
		fw.write("\n\n-------------- classes with different hierarchy --------------\n\n");
		for (String className : differentHierarchy.keySet()) {
			fw.write(className + "\n");
			for (String methodDiff : differentHierarchy.get(className)) {
				fw.write(methodDiff + "\n");
			}
		}
		fw.write("\n\n-------------- classes with different methods --------------\n\n");
		for (String className : differentMethodsClasses.keySet()) {
			fw.write(className + "\n");
			for (String methodDiff : differentMethodsClasses.get(className)) {
				fw.write(methodDiff + "\n");
			}
		}
		fw.write("\n\n-------------- classes with different fields --------------\n\n");
		for (String className : differentFieldsClasses.keySet()) {
			fw.write(className + "\n");
			for (String fieldDiff : differentFieldsClasses.get(className)) {
				fw.write(fieldDiff + "\n");
			}
		}

		fw.close();
	}

	public interface ClassFilter {
		boolean accept(Class<?> candidate);
	}

	public static class NoClassFilter implements ClassFilter {
		@Override
		public boolean accept(final Class<?> candidate) {
			return true;
		}
	}

	public static class NoJunitFilter implements ClassFilter {
		@Override
		public boolean accept(final Class<?> candidate) {
			boolean isTest = false;
			if (candidate.toString().contains("junit")) {
				isTest = true;
			}
			if (candidate.getSuperclass() != null
					&& candidate.getSuperclass().getSimpleName()
							.equals("TestCase")) {
				isTest = true;
			}
			for (Annotation ann : candidate.getAnnotations()) {
				if (ann.annotationType().getCanonicalName().contains("junit")) {
					isTest = true;
					break;
				}
			}
			return !isTest;
		}
	}

	public static class NajaClassFilter extends NoJunitFilter {
		@Override
		public boolean accept(final Class<?> candidate) {
			if (!super.accept(candidate)) {
				return false;
			}
			if (!candidate.isInterface()) {
				return false;
			}
			for (Class<?> inter : candidate.getInterfaces()) {
				if(inter.getCanonicalName().contains("naja.core.components"))
				{
					return true;
				}
			}
			for (Annotation anno : candidate.getAnnotations()) {
				if (anno.annotationType().getCanonicalName().contains("jacomo.components")) {
					return true;
				}
			}
			return false;
		}
	}
}
