package org.boobin.jardiffanalyzer.processing;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.boobin.jardiffanalyzer.Settings;
import org.boobin.jardiffanalyzer.Settings.PackageSubstitution;
import org.boobin.jardiffanalyzer.processing.data.ClassMatchResult;
import org.boobin.jardiffanalyzer.processing.data.entities.ClassDataFactory;
import org.benf.cfr.reader.entities.CfrClassDataFactory;

public class DecompilationUtils {
	
	public interface ComputationProgressListener {
		void notifyProgress(int phase, int progress);
	}

	public static Collection<ClassMatchResult> match(File f1, File f2,
			List<PackageSubstitution> subst, ComputationProgressListener pl) throws IOException {
		List<String> cList1 = new ArrayList<>();
		List<String> cList2 = new ArrayList<>();
		ClassDataFactory fact1 = new CfrClassDataFactory(
				parseClass(f1, cList1), f1);
		ClassDataFactory fact2 = new CfrClassDataFactory(
				parseClass(f2, cList2), f2);
		return match(cList1, cList2, subst, fact1, fact2, pl);
	}
	
	private static int computeProgress(int processedElements, int nbElements) {
		return (int)((double)processedElements / nbElements * 100);
	}

	private static Collection<ClassMatchResult> match(
			Collection<String> cList1, Collection<String> cList2,
			List<PackageSubstitution> subst,
			ClassDataFactory classDataFactory1,
			ClassDataFactory classDataFactory2, ComputationProgressListener pl) {
		Map<String, ClassMatchResult> result = new TreeMap<>();
		int nbElements = cList1.size();
		int processedElements = 0;
		for (String cl : cList1) {
			ClassMatchResult cmr = new ClassMatchResult();
			cmr.classes[0] = classDataFactory1.newClassData(cl);
			if(cmr.classes[0] != null) {
				result.put(
						Settings.fixPackages(cmr.classes[0].getClassName(), subst),
						cmr);
			}
			pl.notifyProgress(0, computeProgress(processedElements++, nbElements));
		}
		nbElements = cList2.size();
		processedElements = 0;
		for (String cl : cList2) {
			ClassMatchResult cmr = result.get(Settings.fixPackages(cl, subst));
			if (cmr == null) {
				cmr = new ClassMatchResult();
				cmr.classes[1] = classDataFactory2.newClassData(cl);
				if(cmr.classes[1] != null) {
					result.put(Settings.fixPackages(cmr.classes[1].getClassName(),
							subst), cmr);
				}
			} else {
				cmr.classes[1] = classDataFactory2.newClassData(cl);
			}
			pl.notifyProgress(1, computeProgress(processedElements++, nbElements));
		}
		nbElements = result.size();
		processedElements = 0;
		for(String name : result.keySet()) {
			try {
				result.get(name).computeMatch(subst);
				pl.notifyProgress(2, computeProgress(processedElements++, nbElements));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result.values();
	}

	private static ClassLoader parseClass(final File file, List<String> result)
			throws IOException {
		List<URL> urls = new ArrayList<>();
		urls.add(file.toURI().toURL());
		parseClassInFileSystem(result, file, "", true, urls);
		return new URLClassLoader(urls.toArray(new URL[0]));
	}

	private static void parseClassInJar(List<String> result, final File file)
			throws IOException {
		ZipFile zipFile = new ZipFile(file);
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if (entry.getName().endsWith(".class")) {
				String tmp = entry.getName().replaceAll("/", ".");
				String className = tmp.substring(0, tmp.lastIndexOf("."));
				result.add(className);
			}
		}
		zipFile.close();
	}

	private static void parseClassInFileSystem(List<String> result,
			final File root, String packageName, final boolean isFirst,
			List<URL> jarFiles) throws IOException {
		if (root.isDirectory()) {
			if (!isFirst) {
				packageName += root.getName() + ".";
			}
			for (File child : root.listFiles()) {
				parseClassInFileSystem(result, child, packageName, false,
						jarFiles);
			}
		} else if (root.getName().endsWith(".class")) {
			String className = packageName
					+ root.getName().substring(0, root.getName().indexOf("."));
			result.add(className);
		} else if (root.getName().endsWith(".jar") || root.getName().endsWith(".war")) {
			jarFiles.add(root.toURI().toURL());
			parseClassInJar(result, root);
		}
	}

}
