package org.boobin.jardiffanalyzer.processing;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.boobin.jardiffanalyzer.Settings;
import org.boobin.jardiffanalyzer.processing.data.ClassContentResult;
import org.boobin.jardiffanalyzer.processing.data.ClassMatchResult;
import org.boobin.jardiffanalyzer.processing.data.MatchResultStatus;

public class ExportFunctions {
	public static void writeHtmlReport(File file, List<ClassMatchResult> data)
			throws IOException {
		if (file.exists()) {
			List<ClassMatchResult> addedClasses = new ArrayList<>();
			List<ClassMatchResult> removedClasses = new ArrayList<>();
			List<ClassMatchResult> modifiedClasses = new ArrayList<>();
			List<ClassMatchResult> identicalClasses = new ArrayList<>();

			preReport(data, addedClasses, removedClasses, modifiedClasses,
					identicalClasses);

			FileWriter fw = new FileWriter(file);
			fw.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
			fw.append("<html>\n");
			fw.append("<body>\n");
			fw.append("<table border=2 width=90%>\n");
			// for (ClassMatchResult cmr : modifiedClasses) {
			// fw.append("<tr>");
			// fw.write("<td><a href=\"#" + cmr.classes[0].getClassName()
			// + "\">" + cmr.classes[0].getClassName()
			// + "</a></td><td><a href=\"#"
			// + cmr.classes[0].getClassName() + "\">"
			// + cmr.classes[1].getClassName() + "</a></td>");
			// fw.append("</tr>");
			// }
			for (ClassMatchResult cmr : removedClasses) {
				fw.append("<tr>\n");
				fw.write("<td>" + cmr.classes[0].getClassName()
						+ "<td></td></td>");
				fw.append("</tr>\n");
			}
			for (ClassMatchResult cmr : addedClasses) {
				fw.append("<tr>\n");
				fw.write("<td></td><td>" + cmr.classes[1].getClassName()
						+ "</td>");
				fw.append("</tr>\n");
			}
			for (ClassMatchResult cmr : modifiedClasses) {
				writeHtmlDetails(fw, cmr);
			}
			fw.append("</table>\n<br><br>\n");

			fw.append("</body>\n");
			fw.append("</html>\n");
			fw.close();
		}
	}
	
	private static void preReport(List<ClassMatchResult> data,
			List<ClassMatchResult> addedClasses,
			List<ClassMatchResult> removedClasses,
			List<ClassMatchResult> modifiedClasses,
			List<ClassMatchResult> identicalClasses) {
		for (ClassMatchResult cmr : data) {
			if (cmr.getStatus() == MatchResultStatus.SAME) {
				identicalClasses.add(cmr);
			} else if (cmr.getStatus() == MatchResultStatus.ADDITION) {
				addedClasses.add(cmr);
			} else if (cmr.getStatus() == MatchResultStatus.SUBSTRACTION) {
				removedClasses.add(cmr);
			} else {
				modifiedClasses.add(cmr);
			}
		}
	}

	private static void writeHtmlDetails(FileWriter fw, ClassMatchResult cmr)
			throws IOException {
		fw.append("<tr><td rowspan=");
		fw.append(Integer.toString(cmr.getNbUnmatchedElements()));
		fw.append('>');
		fw.append(cmr.classes[0].getClassName());
		fw.append("</td>\n");
		writeHtmlData(fw, cmr.content);
	}

	private static void writeHtmlData(FileWriter fw,
			List<? extends ClassContentResult<?>> data) throws IOException {
		boolean isFirst = true;
		for (ClassContentResult<?> fr : data) {
			if (fr.getStatus() != MatchResultStatus.SAME) {
				if (!isFirst) {
					fw.append("<tr>\n");
				} else {
					isFirst = false;
				}
				String s0 = fr.elements[0] == null ? "" : fr.getFullLabel(0);
				String s1 = fr.elements[1] == null ? "" : fr.getFullLabel(1);

				Color color;
				switch (fr.getStatus()) {
					case ADDITION:
					case SUBSTRACTION:
						color = Settings.ADD_OR_REMOVE_COLOR;
						break;
					case IMPL_CHANGED:
						color = Settings.IMPLEM_CHANGED_COLOR;
						break;
					case STRUCTURE_CHANGED:
						color = Settings.STRUCTURE_CHANGED_COLOR;
						break;
					default:
						color = Settings.IDENTICAL_COLOR;
						break;
				}

				String openCell = "<td bgcolor=" + toHexString(color) + ">";
				String closeCell = "</td>";
				fw.write(openCell + fixConstructorNames(s0) + closeCell
						+ openCell + fixConstructorNames(s1) + closeCell);
				fw.append("</tr>\n");
			}
		}
	}

	private static String fixConstructorNames(String in) {
		String tmp = in.replace("<", "&lt;");
		tmp = tmp.replace(">", "&gt;");
		return tmp;
	}

	private static String toHexString(Color c) {
		StringBuilder sb = new StringBuilder("#");

		if (c.getRed() < 16)
			sb.append('0');
		sb.append(Integer.toHexString(c.getRed()));

		if (c.getGreen() < 16)
			sb.append('0');
		sb.append(Integer.toHexString(c.getGreen()));

		if (c.getBlue() < 16)
			sb.append('0');
		sb.append(Integer.toHexString(c.getBlue()));

		return sb.toString();
	}
}
