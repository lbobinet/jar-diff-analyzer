package org.boobin.jardiffanalyzer.processing.data;

import java.io.Serializable;
import java.util.List;

import org.boobin.jardiffanalyzer.Settings;
import org.boobin.jardiffanalyzer.Settings.PackageSubstitution;
import org.boobin.jardiffanalyzer.processing.data.entities.FieldData;
import org.boobin.jardiffanalyzer.processing.data.entities.MethodData;

public class ClassContentResult<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	public T[] elements;

	MatchResultStatus status = MatchResultStatus.UNKNOWN;

	ClassContentResult() {}

	public T getElement(int index) {
		return elements[index];
	}

	public void computeMatch(List<PackageSubstitution> subst) {
		if (elements[0] == null) {
			status = MatchResultStatus.ADDITION;
		} else if (elements[1] == null) {
			status = MatchResultStatus.SUBSTRACTION;
		} else {
			String s1 = Settings.fixPackages(elements[0].toString(), subst);
			String s2 = Settings.fixPackages(elements[1].toString(), subst);
			if (s1.equals(s2)) {
				status = MatchResultStatus.SAME;
			} else {
				status = MatchResultStatus.IMPL_CHANGED;
			}
		}
	}

	public String getShortLabel(int index) {
		return getFullLabel(index);
	}

	public String getFullLabel(int index) {
		return getElement(index) != null ? getElement(index).toString() : "";
	}

	public MatchResultStatus getStatus() {
		return status;
	}

	public static class FieldResult extends ClassContentResult<FieldData> {
		private static final long serialVersionUID = 1L;

		public FieldResult() {
			super();
			elements = new FieldData[2];
		}

		@Override
		public void computeMatch(List<PackageSubstitution> subst) {
			if (elements[0] == null) {
				status = MatchResultStatus.ADDITION;
			} else if (elements[1] == null) {
				status = MatchResultStatus.SUBSTRACTION;
			} else {
				String s1 = Settings.fixPackages(valueField(elements[0])
						.replace('/', '.'), subst);
				String s2 = Settings.fixPackages(valueField(elements[1])
						.replace('/', '.'), subst);
				if (s1.equals(s2)) {
					status = MatchResultStatus.SAME;
				} else {
					status = MatchResultStatus.STRUCTURE_CHANGED;
				}
			}
		}
		
		@Override
		public String getFullLabel(int index) {
			FieldData f = getElement(index);
			return f.getAccessFlags() + " " + f.getType() + " " + f.getName();
		}
	}

	static String valueField(FieldData m) {
		return m.getAccessFlags() + " " + m.getType() + " " + m.getName();
	}

	public static class SuperTypeResult extends ClassContentResult<String> {
		private static final long serialVersionUID = 1L;

		public SuperTypeResult() {
			super();
			elements = new String[2];
		}

		@Override
		public String getFullLabel(int index) {
			return "extends or implements " + super.getFullLabel(index);
		}

	}

	public static class MethodResult extends ClassContentResult<MethodData> {
		private static final long serialVersionUID = 1L;

		public MethodResult() {
			super();
			elements = new MethodData[2];
		}

		@Override
		public void computeMatch(List<PackageSubstitution> subst) {
			if (elements[0] == null) {
				status = MatchResultStatus.ADDITION;
			} else if (elements[1] == null) {
				status = MatchResultStatus.SUBSTRACTION;
			} else {
				if (!(elements[0].getAccessFlags().equals(elements[1]
						.getAccessFlags()))) {
					status = MatchResultStatus.STRUCTURE_CHANGED;
				} else {
					String s1 = Settings.fixPackages(elements[0].getDataToCompare(),
							subst);
					String s2 = Settings.fixPackages(elements[1].getDataToCompare(),
							subst);
					if (s1 == null && s2 == null) {
						status = MatchResultStatus.SAME;
					} else if (s1 == null) {
						status = MatchResultStatus.IMPL_CHANGED;
					} else if (s1.equals(s2)) {
						status = MatchResultStatus.SAME;
					} else {
						status = MatchResultStatus.IMPL_CHANGED;
					}
				}
			}
		}

		@Override
		public String getFullLabel(int index) {
			MethodData m = getElement(index);
			if (m != null) {
				return m.getSignature();
			} else {
				return "";
			}
		}
	}
}
