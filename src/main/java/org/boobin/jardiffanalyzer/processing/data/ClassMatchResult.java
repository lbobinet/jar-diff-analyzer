package org.boobin.jardiffanalyzer.processing.data;

import java.beans.Expression;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.boobin.jardiffanalyzer.Settings;
import org.boobin.jardiffanalyzer.Settings.PackageSubstitution;
import org.boobin.jardiffanalyzer.processing.data.ClassContentResult.FieldResult;
import org.boobin.jardiffanalyzer.processing.data.ClassContentResult.MethodResult;
import org.boobin.jardiffanalyzer.processing.data.ClassContentResult.SuperTypeResult;
import org.boobin.jardiffanalyzer.processing.data.MatchingPolicy.FieldNamingPolicy;
import org.boobin.jardiffanalyzer.processing.data.MatchingPolicy.MethodNamingPolicy;
import org.boobin.jardiffanalyzer.processing.data.MatchingPolicy.SuperTypeNamingPolicy;
import org.boobin.jardiffanalyzer.processing.data.entities.ClassData;
import org.boobin.jardiffanalyzer.processing.data.entities.FieldData;
import org.boobin.jardiffanalyzer.processing.data.entities.MethodData;

public class ClassMatchResult implements Serializable {
	private static final long serialVersionUID = 1L;

	private MatchResultStatus status = MatchResultStatus.UNKNOWN;

	public ClassData[] classes = new ClassData[2];

	public List<ClassContentResult<?>> content = new ArrayList<>();

	@SafeVarargs
	private static <T> void matchElements(
			Map<String, ClassContentResult<T>> result, int index,
			List<PackageSubstitution> subst,
			Class<? extends ClassContentResult<T>> resultType,
			MatchingPolicy<T> namingPolicy, T... elements) {
		if(elements == null) {
			matchElements(result, index, subst, resultType, namingPolicy, new ArrayList<>());
		} else {
			matchElements(result, index, subst, resultType, namingPolicy, Arrays.asList(elements));
		}
	}
	
	@SuppressWarnings("unchecked")
	private static <T> void matchElements(
			Map<String, ClassContentResult<T>> result, int index,
			List<PackageSubstitution> subst,
			Class<? extends ClassContentResult<T>> resultType,
			MatchingPolicy<T> namingPolicy, List<T> elements) {
		for (T f : elements) {
			String id = namingPolicy.idElement(f);
			ClassContentResult<T> fr = result.get(Settings.fixPackages(id,
					subst));
			if (fr == null) {
				try {
					fr = (ClassContentResult<T>) new Expression(resultType, "new",
							new Object[] {}).getValue();
					result.put(Settings.fixPackages(id, subst), fr);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			fr.elements[index] = f;
		}
	}

	public void computeMatch(List<PackageSubstitution> subst) {
		content.clear();
		Map<String, ClassContentResult<MethodData>> mResult = new TreeMap<>();
		Map<String, ClassContentResult<FieldData>> fResult = new TreeMap<>();
		Map<String, ClassContentResult<String>> stResult = new TreeMap<>();

		MethodNamingPolicy mnp = new MethodNamingPolicy();
		FieldNamingPolicy fnp = new FieldNamingPolicy();
		SuperTypeNamingPolicy interfacePolicy = new SuperTypeNamingPolicy(
				"implements ");
		SuperTypeNamingPolicy superClassPolicy = new SuperTypeNamingPolicy(
				"extends ");

		if (classes[0] != null) {
			ClassData clazz = classes[0];
			matchElements(mResult, 0, subst, MethodResult.class, mnp,
					clazz.getMethods());
			matchElements(fResult, 0, subst, FieldResult.class, fnp,
					clazz.getFields());
			if (!clazz.getSuperclassName().equals("java.lang.Object")) {
				matchElements(stResult, 0, subst, SuperTypeResult.class,
						superClassPolicy, clazz.getSuperclassName());
			}
			matchElements(stResult, 0, subst, SuperTypeResult.class,
					interfacePolicy, clazz.getInterfaceNames());
		}
		if (classes[1] != null) {
			ClassData clazz = classes[1];
			matchElements(mResult, 1, subst, MethodResult.class, mnp,
					clazz.getMethods());
			matchElements(fResult, 1, subst, FieldResult.class, fnp,
					clazz.getFields());
			if (!clazz.getSuperclassName().equals("java.lang.Object")) {
				matchElements(stResult, 1, subst, SuperTypeResult.class,
						superClassPolicy, clazz.getSuperclassName());
			}
			matchElements(stResult, 1, subst, SuperTypeResult.class,
					interfacePolicy, clazz.getInterfaceNames());
		}
		content.addAll(stResult.values());
		content.addAll(fResult.values());
		content.addAll(mResult.values());

		for (ClassContentResult<?> mr : content) {
			mr.computeMatch(subst);
		}

		computeStatus();
	}

	private void computeStatus() {
		if (classes[0] == null) {
			status = MatchResultStatus.ADDITION;
		} else if (classes[1] == null) {
			status = MatchResultStatus.SUBSTRACTION;
		} else {
			status = MatchResultStatus.SAME;
			for (ClassContentResult<?> mr : content) {
				if (mr.getStatus() == MatchResultStatus.ADDITION
						|| mr.getStatus() == MatchResultStatus.SUBSTRACTION
						|| mr.getStatus() == MatchResultStatus.STRUCTURE_CHANGED) {
					status = MatchResultStatus.STRUCTURE_CHANGED;
					return;
				} else {
					if (mr.getStatus() == MatchResultStatus.IMPL_CHANGED) {
						status = MatchResultStatus.IMPL_CHANGED;
					}
				}
			}
		}
	}

	public MatchResultStatus getStatus() {
		return status;
	}
	
	public String getShortLabel(int i) {
		ClassData clazz = classes[i];
		if (clazz == null) {
			return null;
		} else {
			String className = clazz.getClassName();
			return className.substring(className.lastIndexOf('.') + 1);
		}
	}
	
	public String getFullLabel(int i) {
		ClassData clazz = classes[i];
		if (clazz == null) {
			return null;
		} else {
			return clazz.getClassName();
		}
	}

	public int getNbUnmatchedElements() {
		return getNbUnmatchedElements(content);
	}

	private int getNbUnmatchedElements(List<ClassContentResult<?>> ccr) {
		int result = 0;
		for (ClassContentResult<?> content : ccr) {
			if (content.getStatus() != MatchResultStatus.SAME) {
				result++;
			}
		}
		return result;
	}
}
