package org.boobin.jardiffanalyzer.processing.data;

public enum MatchResultStatus {
	UNKNOWN, ADDITION, SUBSTRACTION, SAME, STRUCTURE_CHANGED, IMPL_CHANGED
}
