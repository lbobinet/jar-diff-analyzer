package org.boobin.jardiffanalyzer.processing.data;

import org.boobin.jardiffanalyzer.processing.data.entities.FieldData;
import org.boobin.jardiffanalyzer.processing.data.entities.MethodData;

public interface MatchingPolicy<T> {
	String idElement(T element);
	
	class SuperTypeNamingPolicy implements MatchingPolicy<String> {

		private String prefix;

		SuperTypeNamingPolicy(String prefix) {
			this.prefix = prefix;
		}

		@Override
		public String idElement(String element) {
			return prefix + element;
		}
	}

	class MethodNamingPolicy implements MatchingPolicy<MethodData> {
		@Override
		public String idElement(MethodData element) {
			StringBuilder result = new StringBuilder();
			// result.append(Utility.accessToString(m.getAccessFlags()) + " ");
			result.append(element.getReturnType() + " ");
			result.append(element.getName() + "(");
			boolean first = true;
			for (String argType : element.getArgumentTypes()) {
				if (!first) {
					result.append(", ");
				} else {
					first = false;
				}
				result.append(argType);
			}
			result.append(")");
			return result.toString();
		}
	}

	class FieldNamingPolicy implements MatchingPolicy<FieldData> {
		@Override
		public String idElement(FieldData element) {
			return element.getType() + " " + element.getName();
		}
	}

	class DefaultNamingPolicy<T> implements MatchingPolicy<T> {
		@Override
		public String idElement(T element) {
			return element.toString();
		}
	}
}
