package org.boobin.jardiffanalyzer.processing.data.entities;

import java.io.Serializable;
import java.util.List;

public interface ClassData extends Serializable {
	String getClassName();
	List<MethodData> getMethods();
	List<FieldData> getFields();
	String getSuperclassName();
	String[] getInterfaceNames();
	boolean isInterface();
	String getRawCode();
	String cleanCode(String rawCode);
}
