package org.boobin.jardiffanalyzer.processing.data.entities;

public interface ClassDataFactory {
	ClassData newClassData(String name);
}
