package org.boobin.jardiffanalyzer.processing.data.entities;

import java.io.Serializable;

public interface FieldData extends Serializable {
	String getName();
	String getType();
	String getAccessFlags();
}
