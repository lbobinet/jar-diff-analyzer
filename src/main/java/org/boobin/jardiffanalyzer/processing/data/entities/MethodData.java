package org.boobin.jardiffanalyzer.processing.data.entities;

import java.io.Serializable;

public interface MethodData extends Serializable {
	String getReturnType();
	String getName();
	String[] getArgumentTypes();
	String getSignature();
	String getAccessFlags();
	String getDataToCompare();
	String getRawCode(String classRawCode);
	String cleanCode(String rawCode);
}
