package org.boobin.jardiffanalyzer.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.zip.ZipException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableRowSorter;

import org.boobin.jardiffanalyzer.Settings;
import org.boobin.jardiffanalyzer.processing.DecompilationUtils;
import org.boobin.jardiffanalyzer.processing.DecompilationUtils.ComputationProgressListener;
import org.boobin.jardiffanalyzer.processing.ExportFunctions;
import org.boobin.jardiffanalyzer.processing.data.ClassMatchResult;
import org.boobin.jardiffanalyzer.processing.data.MatchResultStatus;
import org.boobin.jardiffanalyzer.ui.components.ClassCellRenderer;
import org.boobin.jardiffanalyzer.ui.components.ClassTableModel;
import org.boobin.jardiffanalyzer.ui.components.DiffPanel;
import org.boobin.jardiffanalyzer.ui.components.SettingsPanel;

import net.miginfocom.swing.MigLayout;

public class ComparatorUI extends JPanel {

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) throws ZipException, IOException {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, fall back to cross-platform
			try {
				UIManager.setLookAndFeel(UIManager
						.getCrossPlatformLookAndFeelClassName());
			} catch (Exception ex) {
			}
		}

		final Settings settings = new Settings();
		settings.importDefault();
		final ComparatorUI ui = new ComparatorUI(settings);
		if (args.length > 1) {
			File f1 = new File(args[0]);
			File f2 = new File(args[1]);
			ui.setFilePath(f1, f2);
			ui.compute(f1, f2);
		}

		final JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.add(ui);
		f.setSize(new Dimension(settings.getWidth(), settings.getHeight()));

		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				try {
					ui.updateSettings();
					settings.exportDefault(f.getWidth(), f.getHeight());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		f.setVisible(true);
	}

	private List<RowFilter<ClassTableModel, Integer>> filters = new ArrayList<>();

	private ClassTableModel model = new ClassTableModel();
	private JTable table = new JTable(model);
	private TableRowSorter<ClassTableModel> sorter;

	private JTextField includeField = new JTextField(20);
	private JTextField excludeField = new JTextField(20);
	private JCheckBox matched = new JCheckBox("identical");
	private JCheckBox added = new JCheckBox("added");
	private JCheckBox removed = new JCheckBox("removed");
	private JCheckBox structChanged = new JCheckBox("structure changed");
	private JCheckBox implChanged = new JCheckBox("implementation changed");
	private JCheckBox interfaces = new JCheckBox("interfaces");
	private JCheckBox classes = new JCheckBox("classes");

	private JTextField file1 = new JTextField();
	private JTextField file2 = new JTextField();

	private Settings settings;

	private ComparatorUI(Settings set) {
		this.settings = set;
		file1.setText(settings.getSrcPath());
		file2.setText(settings.getDestPath());
		table.setCellSelectionEnabled(false);
		sorter = new TableRowSorter<>(model);
		table.setRowSorter(sorter);
		table.setDefaultRenderer(ClassMatchResult.class,
				new ClassCellRenderer());
		final DiffPanel diff = new DiffPanel();

		table.getSelectionModel().addListSelectionListener(e -> {
			if (table.getSelectedRow() != -1) {
				int index = table.convertRowIndexToModel(table
						.getSelectedRow());
				ClassMatchResult row = model.getRowValue(index);
				diff.updateWith(row);
			}
		});

		initFilterComponents();

		JPanel search = new JPanel(new MigLayout("fill, wrap 2",
				"[fill][grow, fill]"));
		search.add(new JLabel("Include"));
		search.add(includeField);
		search.add(new JLabel("Exclude"));
		search.add(excludeField);

		JPanel matchStatus = new JPanel();
		matchStatus.add(matched);
		matchStatus.add(added);
		matchStatus.add(removed);
		matchStatus.add(structChanged);
		matchStatus.add(implChanged);

		JPanel type = new JPanel();
		type.add(interfaces);
		type.add(classes);

		JButton chooseFile1 = new JButton("...");
		chooseFile1.addActionListener(new ChooseFileAction(this, file1));
		JButton chooseFile2 = new JButton("...");
		chooseFile2.addActionListener(new ChooseFileAction(this, file2));

		final JButton settingsBtn = new JButton("Settings");
		settingsBtn.addActionListener(e -> {
			SettingsPanel sp = new SettingsPanel();
			sp.setPackageSubstitutions(settings.getPackageSubstitutions());
			int result = JOptionPane.showConfirmDialog(settingsBtn,
					sp.getComponent(), "Settings",
					JOptionPane.OK_CANCEL_OPTION);
			if (result == JOptionPane.OK_OPTION) {
				settings.setPackageSubstitutions(sp
						.getPackageSubstitutions());
			}
		});
		JPanel topPanel = new JPanel(new MigLayout("fill, wrap 4",
				"[fill][grow, fill][fill][fill]"));
		topPanel.add(new JLabel("src"));
		topPanel.add(file1);
		topPanel.add(chooseFile1);
		topPanel.add(computeButton());
		topPanel.add(new JLabel("dest"));
		topPanel.add(file2);
		topPanel.add(chooseFile2);
		topPanel.add(settingsBtn);

		JSplitPane jsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		jsp.setTopComponent(new JScrollPane(table));
		jsp.setBottomComponent(diff.getComponent());

		JPanel actionsPanel = new JPanel(new MigLayout("fill", "fill"));
		actionsPanel.add(importBtn());
		actionsPanel.add(exportBtn());
		actionsPanel.add(exportHtmlBtn());

		JPanel filterPanel = new JPanel(new MigLayout("fill, wrap 1", "fill"));
		filterPanel.add(matchStatus);
		filterPanel.add(type);

		JPanel bottomPanel = new JPanel(new MigLayout("fill", "fill"));
		bottomPanel.add(search);
		bottomPanel.add(filterPanel);
		bottomPanel.add(actionsPanel);

		setLayout(new BorderLayout());
		add(jsp);
		add(bottomPanel, BorderLayout.SOUTH);
		add(topPanel, BorderLayout.NORTH);
	}

	private void initFilterComponents() {
		ActionListener filterAction = e -> updateFilters();

		DocumentListener fieldChanged = new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				updateFilters();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				updateFilters();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				updateFilters();
			}
		};

		includeField.getDocument().addDocumentListener(fieldChanged);
		excludeField.getDocument().addDocumentListener(fieldChanged);

		matched.setSelected(true);
		added.setSelected(true);
		removed.setSelected(true);
		structChanged.setSelected(true);
		implChanged.setSelected(true);
		interfaces.setSelected(true);
		classes.setSelected(true);
		matched.addActionListener(filterAction);
		added.addActionListener(filterAction);
		removed.addActionListener(filterAction);
		structChanged.addActionListener(filterAction);
		implChanged.addActionListener(filterAction);
		interfaces.addActionListener(filterAction);
		classes.addActionListener(filterAction);
	}

	private JButton exportHtmlBtn() {
		final JButton exportHtmlBtn = new JButton("Export HTML");
		exportHtmlBtn.addActionListener(e ->
				chooseAndWritefile(exportHtmlBtn, new FileNameExtensionFilter(
				"Html files", "html"), file ->
				ExportFunctions.writeHtmlReport(file, getFilteredClasses()))
		);
		return exportHtmlBtn;
	}

	private JButton computeButton() {
		final JButton computeBtn = new JButton("Compute");
		computeBtn.addActionListener(e -> compute(new File(file1.getText()), new File(file2.getText())));
		return computeBtn;
	}

	private JButton exportBtn() {
		final JButton exportBtn = new JButton("Export Data");
		exportBtn.addActionListener(e ->
			chooseAndWritefile(exportBtn, new FileNameExtensionFilter(
				"data files", "dat"), file -> {
				ObjectOutputStream oos = new ObjectOutputStream(
						new FileOutputStream(file));
				oos.writeObject(model.getData());
				oos.close();
			})
		);
		return exportBtn;
	}

	private JButton importBtn() {
		final JButton importBtn = new JButton("Import Data");
		importBtn.addActionListener(e ->
			chooseAndReadfile(importBtn, new FileNameExtensionFilter(
				"data files", "dat"), file -> {
					ObjectInputStream ois = new ObjectInputStream(
							new FileInputStream(file));
					try {
						@SuppressWarnings("unchecked")
						Collection<ClassMatchResult> typedCollection = 
							(Collection<ClassMatchResult>) ois.readObject();
						model.setData(typedCollection, "src", "dest");
					} catch (ClassNotFoundException cnfe) {
						cnfe.printStackTrace();
					}
					ois.close();
				}
			)
		);
		return importBtn;
	}

	private List<ClassMatchResult> getFilteredClasses() {
		List<ClassMatchResult> result = new ArrayList<>();
		int nbVisible = table.getRowCount();
		for (int i = 0; i < nbVisible; i++) {
			int modelIndex = table.convertRowIndexToModel(i);
			result.add(model.getRowValue(modelIndex));
		}

		return result;
	}

	private void setFilePath(File f1, File f2) {
		settings.setSrcPath(f1.getAbsolutePath());
		settings.setDestPath(f2.getAbsolutePath());
		file1.setText(f1.getAbsolutePath());
		file2.setText(f2.getAbsolutePath());
	}

	private void updateSettings() {
		settings.setSrcPath(file1.getText());
		settings.setDestPath(file2.getText());
	}

	private void compute(final File f1, final File f2) {
		if (f1.exists() && f2.exists()) {

			final DefaultComputationProgressListener pl = new DefaultComputationProgressListener(
					(Window) this.getTopLevelAncestor(), 3);

			Executors.newSingleThreadExecutor().submit(() -> {
				try {
					final Collection<ClassMatchResult> data = DecompilationUtils
							.match(f1, f2,
									settings.getPackageSubstitutions(), pl);
					SwingUtilities.invokeLater(() -> {
						model.setData(data, f1.getName(),
								f2.getName());
						pl.setVisible(false);
					});
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			pl.setVisible(true);
		}
	}

	private static class ChooseFileAction implements ActionListener {
		private JTextField tf;
		private ComparatorUI ui;

		ChooseFileAction(ComparatorUI ui, JTextField tf) {
			this.ui = ui;
			this.tf = tf;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser jfc = new JFileChooser();
			if (!tf.getText().isEmpty()) {
				File old = new File(tf.getText());
				if (old.exists()) {
					jfc.setCurrentDirectory(old.getParentFile());
				}
			}
			jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int result = jfc.showOpenDialog(tf);
			if (result == JFileChooser.APPROVE_OPTION) {
				tf.setText(jfc.getSelectedFile().getAbsolutePath());
				ui.updateSettings();
			}
		}
	}

	private void updateFilters() {
		filters.clear();
		if (!matched.isSelected()) {
			filters.add(new RowFilter<ClassTableModel, Integer>() {
				@Override
				public boolean include(
						javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
					ClassMatchResult cmr = entry.getModel().getRowValue(
							entry.getIdentifier());
					return cmr.getStatus() == MatchResultStatus.SAME;
				}
			});
		}
		if (!added.isSelected()) {
			filters.add(new RowFilter<ClassTableModel, Integer>() {
				@Override
				public boolean include(
						javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
					ClassMatchResult cmr = entry.getModel().getRowValue(
							entry.getIdentifier());
					return cmr.getStatus() == MatchResultStatus.ADDITION;
				}
			});
		}
		if (!removed.isSelected()) {
			filters.add(new RowFilter<ClassTableModel, Integer>() {
				@Override
				public boolean include(
						javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
					ClassMatchResult cmr = entry.getModel().getRowValue(
							entry.getIdentifier());
					return cmr.getStatus() == MatchResultStatus.SUBSTRACTION;
				}
			});
		}
		if (!structChanged.isSelected()) {
			filters.add(new RowFilter<ClassTableModel, Integer>() {
				@Override
				public boolean include(
						javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
					ClassMatchResult cmr = entry.getModel().getRowValue(
							entry.getIdentifier());
					return cmr.getStatus() == MatchResultStatus.STRUCTURE_CHANGED;
				}
			});
		}
		if (!implChanged.isSelected()) {
			filters.add(new RowFilter<ClassTableModel, Integer>() {
				@Override
				public boolean include(
						javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
					ClassMatchResult cmr = entry.getModel().getRowValue(
							entry.getIdentifier());
					return cmr.getStatus() == MatchResultStatus.IMPL_CHANGED;
				}
			});
		}
		if (!interfaces.isSelected()) {
			filters.add(new RowFilter<ClassTableModel, Integer>() {
				@Override
				public boolean include(
						javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
					ClassMatchResult cmr = entry.getModel().getRowValue(
							entry.getIdentifier());
					boolean isInterface = false;
					if (cmr.classes[0] != null) {
						isInterface = cmr.classes[0].isInterface();
					}
					if (cmr.classes[1] != null) {
						isInterface |= cmr.classes[1].isInterface();
					}
					return isInterface;
				}
			});
		}
		if (!classes.isSelected()) {
			filters.add(new RowFilter<ClassTableModel, Integer>() {
				@Override
				public boolean include(
						javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
					ClassMatchResult cmr = entry.getModel().getRowValue(
							entry.getIdentifier());
					boolean isInterface = false;
					if (cmr.classes[0] != null) {
						isInterface = cmr.classes[0].isInterface();
					}
					if (cmr.classes[1] != null) {
						isInterface |= cmr.classes[1].isInterface();
					}
					return !isInterface;
				}
			});
		}

		if (!includeField.getText().isEmpty()) {
			filters.add(new ClassMatchPattern(includeField.getText()
					.toLowerCase()));
		}
		if (!excludeField.getText().isEmpty()) {
			filters.add(RowFilter.notFilter(new ClassMatchPattern(excludeField
					.getText().toLowerCase())));
		}
		sorter.setRowFilter(RowFilter.notFilter(RowFilter.orFilter(filters)));
	}

	private static class ClassMatchPattern extends
			RowFilter<ClassTableModel, Integer> {
		private final String pattern;

		ClassMatchPattern(String s) {
			this.pattern = s;
		}

		@Override
		public boolean include(
				javax.swing.RowFilter.Entry<? extends ClassTableModel, ? extends Integer> entry) {
			ClassMatchResult cmr = entry.getModel().getRowValue(
					entry.getIdentifier());
			boolean result = false;
			try {
				Pattern pat = Pattern.compile(".*(" + pattern + ").*");
				if (cmr.classes[0] != null) {
					result = pat.matcher(
							cmr.classes[0].getClassName().toLowerCase())
							.matches();
				}
				if (cmr.classes[1] != null) {
					result |= pat.matcher(
							cmr.classes[1].getClassName().toLowerCase())
							.matches();
				}
			} catch (Exception e) {
			}
			return !result;
		}
	}

	private static void chooseAndWritefile(Component c,
			FileNameExtensionFilter fnef, ActionOnFile action) {

		JFileChooser jfc = new JFileChooser();
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setFileFilter(fnef);

		int result = jfc.showSaveDialog(c);
		if (result == JFileChooser.APPROVE_OPTION) {
			try {
				File file = jfc.getSelectedFile();
				boolean ok = true;
				if (file.exists()) {
					ok = JOptionPane.showConfirmDialog(c, "Overwrite ?") == JOptionPane.YES_OPTION;
					if (ok) {
						file.delete();
						ok = file.createNewFile();
					}
				} else {
					ok = file.createNewFile();
				}
				if (ok) {
					action.action(file);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private static void chooseAndReadfile(Component c,
			FileNameExtensionFilter fnef, ActionOnFile action) {

		JFileChooser jfc = new JFileChooser();
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setFileFilter(fnef);

		int result = jfc.showOpenDialog(c);
		if (result == JFileChooser.APPROVE_OPTION) {
			try {
				File file = jfc.getSelectedFile();
				if (file.exists()) {
					action.action(file);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private interface ActionOnFile {
		void action(File file) throws IOException;
	}

	private static class DefaultComputationProgressListener extends JDialog
			implements ComputationProgressListener {

		private static final long serialVersionUID = 1L;
		private final JProgressBar pb = new JProgressBar(0, 100);
		private final int nbPhases;
		private int currentProgress = 0;

		DefaultComputationProgressListener(Window parent, int nbPhases) {
			super(parent);
			setLocationRelativeTo(parent);
			this.nbPhases = nbPhases;
			add(pb);
			setModalityType(ModalityType.APPLICATION_MODAL);
			pack();
		}

		@Override
		public void notifyProgress(int phase, int progress) {
			int tmp = phase * (100 / nbPhases) + progress / nbPhases;
			if (tmp != currentProgress) {
				currentProgress = tmp;
				SwingUtilities.invokeLater(() -> {
					pb.setValue(currentProgress);
				});
			}
		}
	}
}
