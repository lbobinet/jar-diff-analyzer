package org.boobin.jardiffanalyzer.ui.components;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.boobin.jardiffanalyzer.Settings;
import org.boobin.jardiffanalyzer.processing.data.ClassMatchResult;
import org.boobin.jardiffanalyzer.processing.data.MatchResultStatus;

public class ClassCellRenderer implements TableCellRenderer {

	private DefaultTableCellRenderer delegate = new DefaultTableCellRenderer();

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel c = (JLabel) delegate.getTableCellRendererComponent(table,
				value, isSelected, hasFocus, row, column);
		if (value instanceof ClassMatchResult) {
			c.setText(((ClassMatchResult) value).getShortLabel(column));
			c.setToolTipText(((ClassMatchResult) value).getFullLabel(column));

			if(!table.isRowSelected(row))
			{
				if (((ClassMatchResult) value).getStatus() == MatchResultStatus.SAME) {
					c.setBackground(Settings.IDENTICAL_COLOR);
				} else if (((ClassMatchResult) value).getStatus() == MatchResultStatus.STRUCTURE_CHANGED) {
					c.setBackground(Settings.STRUCTURE_CHANGED_COLOR);
				} else if (((ClassMatchResult) value).getStatus() == MatchResultStatus.IMPL_CHANGED) {
					c.setBackground(Settings.IMPLEM_CHANGED_COLOR);
				} else
				{
					c.setBackground(Settings.ADD_OR_REMOVE_COLOR);
				}
			}
			else {
				c.setBackground(null);
			}
		}
		return c;
	}

}
