package org.boobin.jardiffanalyzer.ui.components;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.boobin.jardiffanalyzer.Settings;
import org.boobin.jardiffanalyzer.processing.data.ClassContentResult;
import org.boobin.jardiffanalyzer.processing.data.MatchResultStatus;

public class ClassContentCellRenderer implements TableCellRenderer {
	private DefaultTableCellRenderer delegate = new DefaultTableCellRenderer();

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel c = (JLabel) delegate.getTableCellRendererComponent(table,
				value, isSelected, hasFocus, row, column);
		if (value instanceof ClassContentResult) {
			Object element = ((ClassContentResult<?>) value).getElement(column);
			if (element != null) {
				c.setText(((ClassContentResult<?>) value).getShortLabel(column));
				c.setToolTipText(((ClassContentResult<?>) value)
						.getFullLabel(column));
			} else {
				c.setText(null);
			}
			
			if(!table.isRowSelected(row))
			{
				if (((ClassContentResult<?>) value).getStatus() == MatchResultStatus.SAME) {
					c.setBackground(Settings.IDENTICAL_COLOR);
				} else if (((ClassContentResult<?>) value).getStatus() == MatchResultStatus.STRUCTURE_CHANGED) {
					c.setBackground(Settings.STRUCTURE_CHANGED_COLOR);
				} else if (((ClassContentResult<?>) value).getStatus() == MatchResultStatus.IMPL_CHANGED) {
					c.setBackground(Settings.IMPLEM_CHANGED_COLOR);
				} else {
					c.setBackground(Settings.ADD_OR_REMOVE_COLOR);
				}
			} else {
				c.setBackground(null);
			}
		} else {
			c.setText(null);
		}
		return c;
	}

}
