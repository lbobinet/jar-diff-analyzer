package org.boobin.jardiffanalyzer.ui.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.swing.table.AbstractTableModel;

import org.boobin.jardiffanalyzer.processing.data.ClassContentResult;
import org.boobin.jardiffanalyzer.processing.data.entities.ClassData;

public class ClassContentTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private List<ClassContentResult<?>> data;
	private String title1 = "left";
	private String title2 = "right";
	
	@Override
	public int getRowCount() {
		return data != null ? data.size() : 0;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}
	
	@Override
	public String getColumnName(int column) {
		switch(column)
		{
			case 0: return title1;
			default : return title2;
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data.get(rowIndex);
	}
	
	void setData(Collection<ClassContentResult<?>> data, Optional<ClassData> cd1, Optional<ClassData> cd2) {
		this.data = new ArrayList<>(data);
		this.title1 = cd1.isPresent() ? cd1.get().getClassName(): "";
		this.title2 = cd2.isPresent() ? cd2.get().getClassName(): "";
		fireTableStructureChanged();
	}
	
	ClassContentResult<?> getRowValue(int rowIndex)
	{
		return data.get(rowIndex);
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return ClassContentResult.class;
	}
}
