package org.boobin.jardiffanalyzer.ui.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.boobin.jardiffanalyzer.processing.data.ClassMatchResult;

public class ClassTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private List<ClassMatchResult> data;
	private String title1 = "src";
	private String title2 = "dest";
	
	@Override
	public int getRowCount() {
		return data != null ? data.size() : 0;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}
	
	public List<ClassMatchResult> getData() {
		return data;
	}
	
	@Override
	public String getColumnName(int column) {
		switch(column)
		{
			case 0: return title1;
			default : return title2;
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data.get(rowIndex);
	}
	
	public void setData(Collection<ClassMatchResult> data, String title1, String title2) {
		this.data = new ArrayList<>(data);
		this.title1 = title1;
		this.title2 = title2;
		fireTableDataChanged();
	}
	
	public ClassMatchResult getRowValue(int rowIndex)
	{
		return data.get(rowIndex);
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return ClassMatchResult.class;
	}
	
}
