package org.boobin.jardiffanalyzer.ui.components;

import java.awt.GridLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Optional;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextPane;

import org.boobin.jardiffanalyzer.processing.data.ClassContentResult;
import org.boobin.jardiffanalyzer.processing.data.ClassMatchResult;
import org.boobin.jardiffanalyzer.processing.data.entities.ClassData;
import org.boobin.jardiffanalyzer.processing.data.entities.MethodData;

public class DiffPanel {

	private ClassContentTableModel methodTableModel = new ClassContentTableModel();

	private JTextPane field1 = new JTextPane();
	private JTextPane field2 = new JTextPane();
	private JScrollPane jsp1 = new JScrollPane(field1);
	private JScrollPane jsp2 = new JScrollPane(field2);
	
	private static class SyncListener implements AdjustmentListener {
		private final JScrollBar otherScrollBar;
		
		SyncListener(JScrollBar otherBar) {
			otherScrollBar = otherBar;
		}
		
		@Override
		public void adjustmentValueChanged(AdjustmentEvent e) {
			otherScrollBar.setValue(Math.min(e.getValue(), otherScrollBar.getMaximum()));
		}
	}

	private Optional<ClassData> cd1 = Optional.empty();
	private Optional<ClassData> cd2 = Optional.empty();

	public JComponent getComponent() {
		final JSplitPane panel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		final JTable table = new JTable(methodTableModel);
		table.setCellSelectionEnabled(false);
		table.setDefaultRenderer(ClassContentResult.class,
				new ClassContentCellRenderer());
		panel.setTopComponent(new JScrollPane(table));

		field1.setEditable(false);
		field2.setEditable(false);
		JPanel botPanel = new JPanel(new GridLayout(1, 2));
		
		jsp1.getVerticalScrollBar().addAdjustmentListener(new SyncListener(jsp2.getVerticalScrollBar()));
		jsp2.getVerticalScrollBar().addAdjustmentListener(new SyncListener(jsp1.getVerticalScrollBar()));
		
		botPanel.add(jsp1);
		botPanel.add(jsp2);

		panel.setBottomComponent(botPanel);

		table.getSelectionModel().addListSelectionListener(e -> {
			if(!e.getValueIsAdjusting())
			{
				if (table.getSelectedRow() != -1) {
					int index = table.convertRowIndexToModel(table
							.getSelectedRow());
					ClassContentResult<?> row = methodTableModel
							.getRowValue(index);
					decompileAndDisplay(row);
				} else {
					displayAllClass();
				}
			}
		});

		panel.addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				if (panel.getDividerLocation() == -1) {
					panel.setDividerLocation(0.5);
				}
			}
		});
		return panel;
	}

	private void displayAllClass() {
		if(cd1.isPresent()) {
			field1.setText(cd1.get().cleanCode(cd1.get().getRawCode()));
		} else {
			field1.setText(null);
		}
		if(cd2.isPresent()) {
			field2.setText(cd2.get().cleanCode(cd2.get().getRawCode()));
		} else {
			field2.setText(null);
		}
	}
	
	private static void displayContent(JTextPane area, Object content, Optional<ClassData> classData) {
		if (content instanceof MethodData) {
			if(classData.isPresent()) {
				MethodData m = (MethodData) content;
				area.setText(m.cleanCode(m.getRawCode(classData.get().getRawCode())));
			}
		}
	}

	private void decompileAndDisplay(ClassContentResult<?> row) {
		displayContent(field1, row.getElement(0), cd1);
		displayContent(field2, row.getElement(1), cd2);
	}

	public void updateWith(ClassMatchResult cmr) {
		cd1 = Optional.ofNullable(cmr.classes[0]);
		cd2 = Optional.ofNullable(cmr.classes[1]);
		methodTableModel.setData(cmr.content, cd1, cd2);
		displayAllClass();
	}

}
