package org.boobin.jardiffanalyzer.ui.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

public class MapTableModel<T> extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	private Map<String, T[]> data;
	private List<String> classNames;
	
	public void setData(Map<String, T[]> data) {
		this.data = data;
		this.classNames = new ArrayList<>(data.keySet());
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return data == null ? 0 : data.size();
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return getRowValues(rowIndex)[columnIndex];
	}
	
	public T[] getRowValues(int rowIndex)
	{
		return data.get(classNames.get(rowIndex));
	}
	
}
