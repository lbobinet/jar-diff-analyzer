package org.boobin.jardiffanalyzer.ui.components;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.boobin.jardiffanalyzer.Settings.PackageSubstitution;

import net.miginfocom.swing.MigLayout;

public class SettingsPanel {

	public static class PackageSubstitutionTableModel extends
			AbstractTableModel {
		private static final long serialVersionUID = 1L;
		
		private static final String[] colNames = new String[]{"Before", "After"};

		private List<PackageSubstitution> data;

		public List<PackageSubstitution> getData() {
			return data;
		}

		public void setData(List<PackageSubstitution> data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getRowCount() {
			return data != null ? data.size() : 0;
		}

		@Override
		public int getColumnCount() {
			return 2;
		}
		
		@Override
		public String getColumnName(int column) {
			return colNames[column];
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
				case 0:
					return data.get(rowIndex).before;
				default:
					return data.get(rowIndex).after;
			}
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return true;
		}

		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			switch (columnIndex) {
				case 0:
					data.get(rowIndex).before = aValue.toString();
					break;
				default:
					data.get(rowIndex).after = aValue.toString();
					break;
			}
		}
	}

	private PackageSubstitutionTableModel model = new PackageSubstitutionTableModel();
	private JTable table = new JTable();
	private JButton addButton = new JButton("+");
	private JButton delButton = new JButton("-");

	public SettingsPanel() {
		addButton.addActionListener(e -> {
			List<PackageSubstitution> tmp = model.getData();
			PackageSubstitution newS = new PackageSubstitution();
			newS.before = "before";
			newS.after = "after";
			tmp.add(newS);
			model.setData(tmp);
		});

		delButton.addActionListener(e -> {
			int index = table.getSelectedRow();
			if (index != -1) {
				model.getData().remove(table.convertRowIndexToModel(index));
				model.fireTableDataChanged();
			}
		});
	}

	public JComponent getComponent() {
		JPanel btnPanel = new JPanel(new MigLayout("fillx, wrap 1", "fill"));
		btnPanel.add(addButton);
		btnPanel.add(delButton);
		JPanel panel = new JPanel(new BorderLayout());
		table.setModel(model);
		panel.setBorder(BorderFactory.createTitledBorder("Package substitution"));
		panel.add(new JScrollPane(table));
		panel.add(btnPanel, BorderLayout.EAST);
		return panel;
	}

	public void setPackageSubstitutions(List<PackageSubstitution> data) {
		model.setData(data);
	}

	public List<PackageSubstitution> getPackageSubstitutions() {
		return model.getData();
	}
}
